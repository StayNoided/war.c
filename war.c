/*
 * 21.c // Un projet Open Source du Blackjack sur C.
 *
 *  Crée: 07 jun. 2021
 *  Fini: XX jun. 2021
 *      Auteur: StayNoided // https://gitlab.com/StayNoided/war.c
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <windows.h>

void Color (int couleurDuTexte, int couleurDeFond){
    HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(H,couleurDeFond*16+couleurDuTexte);
}
int cardDraw(int _iMin, int _iMax){
        /* Fait un chiffre au hasard pour lancer les dés */
	    return (_iMin + (rand () % (_iMax-_iMin+1)));
}
void sleep(unsigned long int n) {
        /* boucle vide parcourue (n * 100000) fois*/
        int i = 0;
        unsigned long int max = n * 100000;
        do {
                /* Faire qqch de stupide qui prend du temps */
                i++;
        }
        while(i <= max);
}

void Card(int diceValue){
    /* Affiche les dés */
	 switch(diceValue)
	    {
	        case 1 :
	        printf(" .------. \n");
	        printf(" |A.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'A| \n");
	        printf(" `------' \n");
	        break;
	        case 2 :
	        printf(" .------. \n");
	        printf(" |2.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'2| \n");
	        printf(" `------' \n");
	        break;
	        case 3 :
	        printf(" .------. \n");
	        printf(" |3.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'3| \n");
	        printf(" `------' \n");
	        break;
	        case 4 :
	        printf(" .------. \n");
	        printf(" |4.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'4| \n");
	        printf(" `------' \n");
	        break;
	        case 5 :
	        printf(" .------. \n");
	        printf(" |5.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'5| \n");
	        printf(" `------' \n");
	        break;
	        case 6 :
	        printf(" .------. \n");
	        printf(" |6.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'6| \n");
	        printf(" `------' \n");
	        break;
	        case 7 :
	        printf(" .------. \n");
	        printf(" |7.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'7| \n");
	        printf(" `------' \n");
	        break;
	        case 8 :
	        printf(" .------. \n");
	        printf(" |8.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'8| \n");
	        printf(" `------' \n");
	        break;
	        case 9 :
	        printf(" .------. \n");
	        printf(" |9.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'9| \n");
	        printf(" `------' \n");
	        break;
	        case 10 :
	        printf(" .------. \n");
	        printf(" |10--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--10| \n");
	        printf(" `------' \n");
	        break;
	        case 11 :
	        printf(" .------. \n");
	        printf(" |V.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'V| \n");
	        printf(" `------' \n");
	        break;
	        case 12 :
	        printf(" .------. \n");
	        printf(" |D.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'D| \n");
	        printf(" `------' \n");
	        break;
	        case 13 :
	        printf(" .------. \n");
	        printf(" |R.--. | \n");
	        printf(" | :  : | \n");
	        printf(" | :  : | \n");
	        printf(" | '--'R| \n");
	        printf(" `------' \n");
	        break;
	        case 14 :
	        printf(" .------. \n");
	        printf(" |:'::':| \n");
	        printf(" |.:..:.| \n");
	        printf(" |:'::':| \n");
	        printf(" |::..::| \n");
	        printf(" `------' \n");
	        break;


	    }

}
void main()
{
    int cards[3][52],i,j,k=0,k2=0,t;/*setup cartes au debut */
    int menuChoice, waitCycle,cardCount;
	int c1, c2, c3, c4, c5, c6, piocheJ=0, scoreP ,ValeurP,stockP=0, totalP=26; /* ints Joueurs */
	int o1, o2, o3, o4, o5, o6, piocheO=0, ValeurO, scoreO,stockO=0, totalO=26; /* Ordis */
	int parties, cont,meta, Valeur, pick,nbCartes;
	char c;
    int stime;
    for (i=1;i<5;i++){
        for(j=0;j<13;j++){
            cards[0][(j+(13*(i-1)))]=j;
            cards[1][(j+(13*(i-1)))]=i;
            if (i == 1){
            cards[2][(j+(13*(i-1)))]=0;
            } else if (i == 3){
            cards[2][(j+(13*(i-1)))]=0;
            }
            else{
            cards[2][(j+(13*(i-1)))]=1;
            }
            /*printf("c %d s %d j %d \n",cards[0][(j+(13*(i-1)))]+1,cards[1][(j+(13*(i-1)))],cards[2][(j+(13*(i-1)))]);*/
        }

    }

    Color(2,0);
    printf("Tabby's\n");
    sleep(400);
    printf("888       888                  888\n");
    sleep(400);
    printf("888       888                  888\n");
    sleep(400);
    printf("888  d8b  888                  888\n");
    sleep(400);
    printf("888 d888b 888  8888b.  888d888 888\n");
    sleep(400);
    printf("888d88888b888     '88b 888P'   888\n");
    sleep(400);
    printf("88888P Y88888 .d888888 888     Y8P\n");
    sleep(400);
    printf("8888P   Y8888 888  888 888      * \n");
    sleep(400);
    printf("888P     Y888 *Y888888 888     888\n");
    sleep(700);
    printf("\n");
    printf("Ceci est un jeu de bataille qui est disponible en open-source sur https://gitlab.com/StayNoided/WAR.c\n");
    Color(8,0);
    printf("The source code is available at https://gitlab.com/StayNoided/WAR.c\n\n\n");
    Color(3,0);

    waitCycle=0;
    scoreO = 0;
    scoreP = 0;
    parties=0;
    cont=1;
    meta=1;
    while (cont==1){
        while(meta==1){
                getch();
        srand(time(NULL));
        Color(3,0);
        printf("\n\n\n\n\n\n\nVotre pioche:\n");
        Color(8,0);
        printf("Your pick:\n");
        piocheJ=0;

        while(piocheJ==0){
                c1=cardDraw(0,51);


            if(cards[2][c1]==0 && cards[0][c1]>0){

            Card(cards[0][c1]);
            piocheJ=1;
            sleep(400);
            if (cards[1][c1]==1){ /* Affichage Texte */
                Color(3,0);
                printf("%d de Trefle\n",cards[0][c1]);
                Color(8,0);
                printf("%d of Clubs\n",cards[0][c1]);
                Color(3,0);
            } else if (cards[1][c1]==2){
                Color(3,0);
                printf("%d de Pique\n",cards[0][c1]);
                Color(8,0);
                printf("%d of Spades\n",cards[0][c1]);
                Color(3,0);
            } else if (cards[1][c1]==3){
                Color(3,0);
                printf("%d de Coeur\n",cards[0][c1]);
                Color(8,0);
                printf("%d of Hearts\n",cards[0][c1]);
                Color(3,0);
            } else if (cards[1][c1]==4){
                Color(3,0);
                printf("%d de Carreau\n",cards[0][c1]);
                Color(8,0);
                printf("%d of Diamonds\n",cards[0][c1]);
                Color(3,0);
            }
            }


        }
        Color(2,0);
        sleep(400);
        printf("Pioche de l'ordinateur:\n");
        Color(8,0);
        printf("COM's pick:\n");
        piocheO=0;
        while(piocheO==0){
               o1=cardDraw(0,51);
            if(cards[2][o1]==1 && cards[0][o1]>0){
            sleep(400);
            Card(cards[0][o1]);
            piocheO=1;
            sleep(400);
            if (cards[1][o1]==1){ /* Affichage Texte */
                Color(2,0);
                printf("%d de Trefle\n",cards[0][o1]);
                Color(8,0);
                printf("%d of Clubs\n",cards[0][o1]);
                Color(2,0);
            } else if (cards[1][o1]==2){
                Color(2,0);
                printf("%d de Pique\n",cards[0][o1]);
                Color(8,0);
                printf("%d of Spades\n",cards[0][o1]);
                Color(2,0);
            } else if (cards[1][o1]==3){
                Color(2,0);
                printf("%d de Coeur\n",cards[0][o1]);
                Color(8,0);
                printf("%d of Hearts\n",cards[0][o1]);
                Color(2,0);
            } else if (cards[1][o1]==4){
                Color(2,0);
                printf("%d de Carreau\n",cards[0][o1]);
                Color(8,0);
                printf("%d of Diamonds\n",cards[0][o1]);
                Color(2,0);
            }

            }


        }
        if(cards[0][c1]>cards[0][o1]){
            printf("%d est plus grand que %d, le joueur gagne le tour\n",cards[0][c1],cards[0][o1]);
            Color(8,0);
            printf("%d is greater than %d, player wins the round\n",cards[0][c1],cards[0][o1]);
            cards[2][c1]=2;
            cards[2][o1]=2;
            totalP--;
            totalO--;
            stockP++;
            stockP++;
        }else if(cards[0][c1]<cards[0][o1]){
            printf("%d est plus grand que %d, l'ordi gagne le tour\n",cards[0][o1],cards[0][c1]);
            Color(8,0);
            printf("%d is greater than %d, COM wins the round\n",cards[0][c1],cards[0][o1]);
            cards[2][c1]=3;
            cards[2][o1]=3;
            totalP--;
            totalO--;
            stockO++;
            stockO++;
            }
        else{
            printf("BATAILLE! Appuyez sur [ENTREE] pour continuer.\n");
            Color(8,0);
            printf("WAR! Press [ENTER] to continue.\n");
            getch();
            /*Bataille*/
        Color(3,0);
        printf("\n\n\n\n\nVotre pioche:\n");
        Color(8,0);
        printf("Your pick:\n");
        piocheJ=0;
        while(piocheJ==0){
                c2=cardDraw(0,51);


            if(cards[2][c2]==0 && cards[0][c2]>2){

            piocheJ=1;
            }
        }
        piocheJ=0;
        while(piocheJ==0){
                c3=cardDraw(0,51);


            if(cards[2][c3]==0 && cards[0][c3]>0){
            Card(14);
            Card(cards[0][c3]);
            piocheJ=1;
            sleep(400);
            }
        }
            if (cards[1][c2]==1){ /* Affichage Texte */
                Color(3,0);
                printf("(%d de Trefle)\n",cards[0][c2]);
                Color(8,0);
                printf("(%d of Clubs)\n",cards[0][c2]);
                Color(3,0);
            } else if (cards[1][c2]==2){
                Color(3,0);
                printf("(%d de Pique)\n",cards[0][c2]);
                Color(8,0);
                printf("(%d of Spades)\n",cards[0][c2]);
                Color(3,0);
            } else if (cards[1][c2]==3){
                Color(3,0);
                printf("(%d de Coeur)\n",cards[0][c2]);
                Color(8,0);
                printf("(%d of Hearts)\n",cards[0][c2]);
                Color(3,0);
            } else if (cards[1][c2]==4){
                Color(3,0);
                printf("(%d de Carreau)\n",cards[0][c2]);
                Color(8,0);
                printf("(%d of Diamonds)\n",cards[0][c2]);
                Color(3,0);
            }
            if (cards[1][c3]==1){ /* Affichage Texte */
                Color(3,0);
                printf("%d de Trefle\n",cards[0][c3]);
                Color(8,0);
                printf("%d of Clubs\n",cards[0][c3]);
                Color(3,0);
            } else if (cards[1][c3]==2){
                Color(3,0);
                printf("%d de Pique\n",cards[0][c3]);
                Color(8,0);
                printf("%d of Spades\n",cards[0][c3]);
                Color(3,0);
            } else if (cards[1][c3]==3){
                Color(3,0);
                printf("%d de Coeur\n",cards[0][c3]);
                Color(8,0);
                printf("%d of Hearts\n",cards[0][c3]);
                Color(3,0);
            } else if (cards[1][c3]==4){
                Color(3,0);
                printf("%d de Carreau\n",cards[0][c3]);
                Color(8,0);
                printf("%d of Diamonds\n",cards[0][c3]);
                Color(3,0);
            }



        Color(2,0);
        sleep(400);
        printf("Pioche de l'ordinateur:\n");
        Color(8,0);
        printf("COM's pick:\n");
        piocheO=0;
        while(piocheO==0){
            o2=cardDraw(0,51);
            if(cards[2][o2]==1 && cards[0][o2]>0){
            piocheO=1;
            }
        }
        piocheO=0;
        while(piocheO==0){
            o3=cardDraw(0,51);
            if(cards[2][o3]==1 && cards[0][o3]>0){
            sleep(400);
            Card(14);
            Card(cards[0][o3]);
            piocheO=1;
            sleep(400);
            }
        }
            if (cards[1][o2]==1){ /* Affichage Texte */
                Color(2,0);
                printf("(%d de Trefle)\n",cards[0][o2]);
                Color(8,0);
                printf("(%d of Clubs)\n",cards[0][o2]);
                Color(2,0);
            } else if (cards[1][o2]==2){
                Color(2,0);
                printf("(%d de Pique)\n",cards[0][o2]);
                Color(8,0);
                printf("(%d of Spades)\n",cards[0][o2]);
                Color(2,0);
            } else if (cards[1][o2]==3){
                Color(2,0);
                printf("(%d de Coeur)\n",cards[0][o2]);
                Color(8,0);
                printf("(%d of Hearts)\n",cards[0][o2]);
                Color(2,0);
            } else if (cards[1][o2]==4){
                Color(2,0);
                printf("(%d de Carreau)\n",cards[0][o2]);
                Color(8,0);
                printf("(%d of Diamonds)\n",cards[0][o2]);
                Color(2,0);
            }
            if (cards[1][o3]==1){ /* Affichage Texte */
                Color(2,0);
                printf("%d de Trefle\n",cards[0][o3]);
                Color(8,0);
                printf("%d of Clubs\n",cards[0][o3]);
                Color(2,0);
            } else if (cards[1][o3]==2){
                Color(2,0);
                printf("%d de Pique\n",cards[0][o3]);
                Color(8,0);
                printf("%d of Spades\n",cards[0][o3]);
                Color(2,0);
            } else if (cards[1][o3]==3){
                Color(2,0);
                printf("%d de Coeur\n",cards[0][o3]);
                Color(8,0);
                printf("%d of Hearts\n",cards[0][o3]);
                Color(2,0);
            } else if (cards[1][o3]==4){
                Color(2,0);
                printf("%d de Carreau\n",cards[0][o3]);
                Color(8,0);
                printf("%d of Diamonds\n",cards[0][o3]);
                Color(2,0);
            }



        if(cards[0][c3]>cards[0][o3]){
            Color(2,0);
            printf("%d est plus grand que %d, le joueur gagne le tour\n",cards[0][c3],cards[0][o3]);
            Color(8,0);
            printf("%d is greater than %d, player wins the round\n",cards[0][c3],cards[0][o3]);
            cards[2][c1]=2;
            cards[2][c2]=2;
            cards[2][c3]=2;
            cards[2][o1]=2;
            cards[2][o2]=2;
            cards[2][o3]=2;
            totalP=totalP-3;
            totalO=totalO-3;
            stockP=stockP+6;
        }else if(cards[0][c3]<cards[0][o3]){
            Color(2,0);
            printf("%d est plus grand que %d, l'ordi gagne le tour\n",cards[0][o3],cards[0][c3]);
            Color(8,0);
            printf("%d is greater than %d, COM wins the round\n",cards[0][c3],cards[0][o3]);
            cards[2][c1]=3;
            cards[2][c2]=3;
            cards[2][c3]=3;
            cards[2][o1]=3;
            cards[2][o2]=3;
            cards[2][o3]=3;
            totalP=totalP-3;
            totalO=totalO-3;
            stockO=stockO+6;
            }
        else{
            printf("Egalite! Les cartes retournent dans leur pioche respective\nAppuyez sur [ENTREE] pour continuer.\n");
            Color(8,0);
            printf("Tie! The cards go back in their respective decks\nPress [ENTER] to continue.\n");
            cards[2][c1]=2;
            cards[2][c2]=2;
            cards[2][c3]=2;
            cards[2][o1]=3;
            cards[2][o2]=3;
            cards[2][o3]=3;
            totalP=totalP-3;
            totalO=totalO-3;
            stockP=stockP+3;
            stockO=stockO+3;



            }
        }
        if (totalP<3){
            meta=0;
        }
        if (totalO<3){
            meta=0;
        }
        }
        Color(3,0);

        printf("Voulez vous continuer?\n");
        Color(8,0);
        printf("Would you like to continue?\n");
        printf("[");
        Color(2,0);
        printf("O (Y)");
        Color(8,0);
        printf(" / ");
        Color(1,0);
        printf("N");
        Color(8,0);
        printf("]\n");
        c = 'x';
        c = getc(stdin);
        if(c=='n'||c=='N'){
        cont=0;
        }else if(c=='y'||c=='Y'||c=='o'||c=='O'){
        Color(2,0);
        printf("Appuyez sur [ENTREE] pour continuer\n");
        Color(8,0);
        printf("Press [ENTER] to continue\n");
        Color(2,0);
        meta=1;
        }
        for (i=0;i<52;i++){
            if (cards[2][i] == 2){
            cards[2][i]=0;
            totalP++;
            stockP--;
            } else if (cards[2][i] == 3){
            cards[2][i]=1;
            totalO++;
            stockO--;

            }
        }


        }
        if (totalP>totalO){
        Color(3,0);
        printf("Le joueur a gagne avec %d cartes\n",totalP);
        Color(8,0);
        printf("The player won with a total of %d cards\n",totalP);
        getch();
        }else if(totalO>totalP){
        Color(3,0);
        printf("L'ordi a gagne avec %d cartes\n",totalO);
        Color(8,0);
        printf("COMP won with a total of %d cards\n",totalO);
        getch();
        }
        else{
         Color(3,0);
        printf("Egalite.\n",totalO);
        Color(8,0);
        printf("Tie.\n",totalO);
        }
    }







